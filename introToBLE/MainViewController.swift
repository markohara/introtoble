import UIKit
import Bluejay

class MainViewController: UIViewController, OTAClientDelegate {
    private var otaClient = OTAClient.shared
    var continueAction: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        otaClient.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        continueBtn.isHidden = true
    }

    @IBOutlet weak var continueBtn: UIButton!
    @IBOutlet weak var label: UILabel!
    
    @IBAction func perform(_ sender: Any) {
        performOTA()
    }

    @IBAction func continuePressed(_ sender: Any) {
        if let action = continueAction {
            action()
            continueBtn.isHidden = true
        }
    }
    
    func message(_ message: String) {
        self.label.text = message
    }
    
    // function only called twice throughout the application,
    // temp way to stall for user input
    func dispatchAlert(action: (() -> Void)?) {
        if let action = action {
            continueBtn.isHidden = false
            continueAction = action
        } else {
            alertOne()
        }
    }
    
    func alertOne() {
        let avc = UIAlertController(title: "Forget Device",
                                    message: "Please go to settings and forget \(OTAClient.shared.selectedFirmware?.advertName ?? ""), press \"Ok\" when done ", preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Ok", style: .default, handler: { _ in
            OTAClient.shared.processTwo()
        })
        avc.addAction(okAction)
        self.present(avc, animated: true, completion: nil)
    }
    
    func performOTA() {
        let avc = UIAlertController(title: "Firmware Options", message: "Select the firmware to install, a scan will begin for the correct device.", preferredStyle: .actionSheet)

        Firmware.allCases.forEach{ firmware in
            let action = UIAlertAction(title: firmware.name, style: .default, handler: { _ in
                self.otaClient.selectedFirmware = firmware
                self.otaClient.process()
            })
            avc.addAction(action)
        }
        
        let dismiss = UIAlertAction(title: "cancel", style: .cancel, handler: { _ in
            avc.dismiss(animated: true, completion: nil)
        })
        avc.addAction(dismiss)
        
        present(avc, animated: true, completion: nil)
    }
}

