import UIKit
import Bluejay

// Delegate only used to output to the screen and halt for user input
protocol OTAClientDelegate {
    func message(_ message: String)
    func dispatchAlert(action: (() -> Void)?)
}

class OTAClient: DisconnectHandler, OTAClientDelegate {
    private var chain: Handler? // used until DFU is complete
    private var chainTwo: Handler? // used for everything after DFU
    static let shared = OTAClient()
    
    private var oneCompleted = false
    
    var selectedFirmware: Firmware?
    var delegate: OTAClientDelegate?
    
    var ota: OTAHandler?
    private init() {
        BLEManager.shared.bluejay.registerDisconnectHandler(handler: self)
        
        // chain one = scan(for appLoader device) -> connect(to appLoader device) -> DFU
        var scan = ScanHandler()
        var conn = ConnectionHandler()
        let dfu = DFUModeHandler()
        conn.setNext(next: dfu)
        scan.setNext(next: conn)
        
        chain = scan
        
        // chain Two = forgetDevice(waits for forget) -> scan(for bootloader device) -> connect(to bootloader device) -> OTA flow (defined in OTAHandler.swift)
        let ota = OTAHandler()
        var dfuConn = ConnectionHandler()
        var dfuScan = ScanHandler()
        
        dfuConn.setNext(next: ota)
        dfuScan.setNext(next: dfuConn)
        chainTwo = dfuScan
    }
    
    func process() {
        print("process one handler")
        guard let firmware = selectedFirmware else {
            return
        }
        self.chain!.handle(deviceName: firmware.advertName, per: nil, firmware: firmware)
        oneCompleted = true
    }
    
    func processTwo() {
        guard oneCompleted else {
            return
        }
        print("process two handler")
        // unregisters disconnect handler to ensure we dont call it after the dfu disconnect
        BLEManager.shared.bluejay.unregisterDisconnectHandler()

        guard let firmware = selectedFirmware else {
            return
        }
        
        self.chainTwo!.handle(deviceName: firmware.bootloaderName, per: nil, firmware: firmware)
    }
    
    func didDisconnect(from peripheral: PeripheralIdentifier, with error: Error?, willReconnect autoReconnect: Bool) -> AutoReconnectMode {
        //presents an alert in MainViewController, which when acknowledged invokes processTwo()
        dispatchAlert(action: nil)
        
        return .change(shouldAutoReconnect: false)
    }
    
    // OTAClientDelegate Methods
    func message(_ message: String) {
        self.delegate?.message(message)
    }
    
    func dispatchAlert(action: (() -> Void)?) {
        self.delegate?.dispatchAlert(action: action)
    }
    
}
