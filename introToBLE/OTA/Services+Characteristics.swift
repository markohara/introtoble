import Foundation
import Bluejay

enum services {
    case modius, ota
    
    var id: ServiceIdentifier {
        switch self {
        case .modius:
            return ServiceIdentifier(uuid: "63183e10-b197-11e6-9598-0800200c9a66")
        case .ota:
            return ServiceIdentifier(uuid: "1d14d6ee-fd63-4fa1-bfa4-8f47b42119f0")
        }
    }
}

enum otaCharacteristics {
    case control, data
    
    var id: CharacteristicIdentifier {
        switch self {
        case .control:
            return CharacteristicIdentifier(uuid: "f7bf3564-fb6d-4e53-88a4-5e37e0326063", service: services.ota.id)
        case .data:
            return CharacteristicIdentifier(uuid: "984227f3-34fc-4045-a5d0-2c581f81a153", service: services.ota.id)
        }
    }
}
