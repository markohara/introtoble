import Foundation
import Bluejay

struct InitiateWriteHandler: Handler {
    var next: Handler?
    
    func handle(deviceName: String, per: PeripheralIdentifier?, firmware: Firmware) {
        OTAClient.shared.message("Signaling start of file")

        BLEManager.shared.bluejay.write(to: otaCharacteristics.control.id, value: UInt8(0x00), type: .withResponse) { result in
            switch result {
            case .success:
                print("succesfully initated file transfer in initiated write handler")
                self.next?.handle(deviceName: deviceName, per: per, firmware: firmware)
            case .failure(let error):
                print("failure in initiated write handler, \(error.localizedDescription)")
            }
        }
    }
}
