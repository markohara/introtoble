import Foundation
import Bluejay

class FileWriteHandler: Handler {
    var next: Handler?
    var packets = [Data]()
    var currentPacket = 0
    
    var deviceName = ""
    var per: PeripheralIdentifier?
    var firmware = Firmware.modius(.new)
    
    func handle(deviceName: String, per: PeripheralIdentifier?, firmware: Firmware) {
        self.deviceName = deviceName
        self.per = per
        self.firmware = firmware
        
        guard let firmwareFile = firmware.loadFirmware() else {
            return
        }
        
        // chunks implementation can be found in Data+Chunks.swift
        self.packets = firmwareFile.chunks(of: firmware.MaximumByteSizeForDevice)
        
        uploadPacket()
    }
    
    func uploadPacket() {
        if currentPacket < self.packets.count {
            print("Handler packer \(currentPacket) / \(packets.count - 1)")
            let percent = ((Double(self.currentPacket) / Double(self.packets.count - 1)) * 100)
            OTAClient.shared.message("uploading \(Int(percent.rounded()))%")
            
            //Pads current packet if needed
            let packet = packets[currentPacket].toBluetoothData()
            BLEManager.shared.bluejay.write(to: otaCharacteristics.data.id,
                                            value: packet,
                                            type: .withoutResponse,
                                            completion: { _ in
                self.currentPacket += 1
                self.ready()
            })
        }
    }
    
    func ready() {
        if currentPacket < self.packets.count {
            uploadPacket()
        } else {
            next?.handle(deviceName: self.deviceName, per: self.per, firmware: self.firmware)
        }
    }

}
