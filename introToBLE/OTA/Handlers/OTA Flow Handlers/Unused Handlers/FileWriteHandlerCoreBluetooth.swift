//import Foundation
//import Bluejay
//import CoreBluetooth
//
//class FileWriteHandler: NSObject, Handler, CBCentralManagerDelegate, CBPeripheralDelegate, CBPeripheralManagerDelegate {
//    var next: Handler?
//    
//    var packets: [Data] = []
//    
//    var cbCentralManager: CBCentralManager?
//    var peripherial: CBPeripheral?
//    
//    let service = services.ota.id
//    
//    var otaService: CBService?
//    var controlChar: CBCharacteristic?
//    var dataChar: CBCharacteristic?
//    
//    var currentPacket = 0
//
//    private func setUp() {
//        OTAClient.shared.message("Handing over to core bluetooth")
//
//        let coreble = BLEManager.shared.bluejay.stopAndExtractBluetoothState()
//        self.cbCentralManager = coreble.manager
//        self.peripherial = coreble.peripheral
//        
//        self.cbCentralManager?.delegate = self
//        self.peripherial?.delegate = self
//        
//        if let per = self.peripherial {
//            discoverServices(peripheral: per)
//        }
//        print("set up")
//    }
//    
//    func tearDown() {
//        guard let manager = self.cbCentralManager else {
//            return
//        }
//        BLEManager.shared.bluejay.start(mode: .use(manager: manager, peripheral: self.peripherial))
//    }
//    
//    func handle(deviceName: String, per: PeripheralIdentifier?, firmware: Firmware) {
//        guard let firmwareFile = firmware.loadFirmware() else {
//            return
//        }
//        // chunks implementation can be found in Data+Chunks.swift
//        self.packets = firmwareFile.chunks(of: firmware.MaximumByteSizeForDevice)
//        
//        // Five second wait to ensure bluejay operation queue is empty
//        DispatchQueue.main.asyncAfter(deadline: .now() + 5, execute: {
//            self.setUp()
//            // ensures ropey service discovery is complete
//            DispatchQueue.main.asyncAfter(deadline: .now() + 5, execute: {
//                self.uploadPacket()
//            })
//        })
//    }
//
//    func uploadPacket() {
//        if currentPacket < self.packets.count {
//            // This check ensures we can actually perform a write, if we didnt do this if the buffer
//            // is full the OS could just throw packets away, if false it calls
//            // peripheralIsReady when we can send the next packet
//            if self.peripherial?.canSendWriteWithoutResponse ?? false {
//                print("Handler packer \(currentPacket) / \(packets.count - 1)")
//                let percent = ((Double(self.currentPacket) / Double(self.packets.count - 1)) * 100)
//                OTAClient.shared.message("uploading \(Int(percent.rounded()))%")
//                
//                //Pads current packet if needed
//                let packet = packets[currentPacket].toBluetoothData()
//                peripherial?.writeValue(packet, for: dataChar!, type: .withoutResponse)
//                currentPacket += 1
//            }
//        }
//    }
//    
//    func peripheralIsReady(toSendWriteWithoutResponse peripheral: CBPeripheral) {
//        if currentPacket < self.packets.count {
//            uploadPacket()
//        } else {
//            OTAClient.shared.message("Signaling end of file")
//            // Signaling end of file
//            self.writeSingleByte(value: UInt8(0x03), to: controlChar!)
//            
//            //quick way to make sure the last write has actually happened
//            DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
//                OTAClient.shared.message("Disconnecting from device")
//                self.cbCentralManager?.cancelPeripheralConnection(self.peripherial!)
//            })
//        }
//    }
//    
//    func writeSingleByte(value: UInt8, to: CBCharacteristic) {
//        let data = Data(bytes: [value], count: 1)
//        self.peripherial?.writeValue(data, for: to, type: .withResponse)
//    }
//
//    func discoverServices(peripheral: CBPeripheral) {
//        peripheral.discoverServices(nil)
//    }
//    
//    func discoverCharacteristics(peripheral: CBPeripheral) {
//        guard let services = peripheral.services else {
//            return
//        }
//        for service in services {
//            peripheral.discoverCharacteristics(nil, for: service)
//        }
//    }
//    
//    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
//        guard let _ = peripheral.services else {
//            return
//        }
//        discoverCharacteristics(peripheral: peripheral)
//    }
//    
//    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
//        guard let characteristics = service.characteristics else {
//            return
//        }
//        
//        if service.uuid == self.service.uuid {
//            self.otaService = service
//            self.controlChar = characteristics[0]
//            self.dataChar = characteristics[1]
//            return
//        }
//    }
//    
//    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
//        OTAClient.shared.message("OTA transfer complete🎉 \n\nTo go again force quit the app and relaunch")
//    }
//    
//    func centralManagerDidUpdateState(_ central: CBCentralManager) {
//        
//    }
//    
//    func peripheralManagerDidUpdateState(_ peripheral: CBPeripheralManager) {
//    }
//}
