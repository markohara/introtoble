import Foundation
import Bluejay

struct TerminateWriteHandler: Handler {
    var next: Handler?
    
    func handle(deviceName: String, per: PeripheralIdentifier?, firmware: Firmware) {
        OTAClient.shared.message("Signaling end of file")
        BLEManager.shared.bluejay.write(to: otaCharacteristics.control.id, value: UInt8(0x03), type: .withResponse) { result in
            switch result {
            case .success:
                print("succesfully terminated write in terminate write handler")
                self.next?.handle(deviceName: deviceName, per: per, firmware: firmware)
            case .failure(let error):
                print("failure in terminate write handler, \(error.localizedDescription)")
            }
        }
    }
}
