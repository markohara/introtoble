import Foundation
import Bluejay

class OTAHandler: Handler {
    
    var next: Handler?
    
    func handle(deviceName: String, per: PeripheralIdentifier?, firmware: Firmware) {
        // configure OTA chain
        let closeConnection = CloseConnectionHandler()
        var terminateWrite = TerminateWriteHandler()
        var fileWrite = FileWriteHandler()
        var initiateWrite = InitiateWriteHandler()
        
        // build chain signal write beginning -> file write -> signal write ending -> close connection
        terminateWrite.setNext(next: closeConnection)
        fileWrite.setNext(next: terminateWrite)
        initiateWrite.setNext(next: fileWrite)
        
        next = initiateWrite
        
        self.next?.handle(deviceName: deviceName, per: per, firmware: firmware)
    }
}
