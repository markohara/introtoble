import Foundation
import Bluejay

class CloseConnectionHandler: Handler, DisconnectHandler {
    var next: Handler?
    
    func handle(deviceName: String, per: PeripheralIdentifier?, firmware: Firmware) {
        print("inside close connection handler")
        BLEManager.shared.bluejay.registerDisconnectHandler(handler: self)
        
        BLEManager.shared.bluejay.disconnect(immediate: false) { result in
            switch result {
            case .disconnected:
                print("succesfully closed connection in close connection handler")
                return
            case .failure(let error):
                print("failure in close connection handler, \(error.localizedDescription)")
            }
        }
    }
    
    func setNext(next: Handler) {
        fatalError("Cannot set next as this handler terminates the connection")
    }
    
    func didDisconnect(from peripheral: PeripheralIdentifier, with error: Error?, willReconnect autoReconnect: Bool) -> AutoReconnectMode {
        OTAClient.shared.message("""
OTA transfer complete🎉

To go again force quit the app and relaunch
""")
        return .change(shouldAutoReconnect: false)
    }
}
