import Foundation
import Bluejay
import CoreBluetooth

struct ScanHandler: Handler {
    var next: Handler?
    
    func handle(deviceName: String, per: PeripheralIdentifier?, firmware: Firmware) {
        print("inside Scan Handler")
        OTAClient.shared.message("Scanning for \(deviceName)")
        BLEManager.shared.scan { peripherals in
            guard peripherals.count > 0 else {
                return false
            }

            let pers = peripherals.filter {
                ($0.advertisementPacket["kCBAdvDataLocalName"] as? String)?.lowercased() == deviceName.lowercased()
            }
            
            if let id = pers.first {
                print("Scan Handler - Successfully found \(deviceName)")
                BLEManager.shared.bluejay.stopScanning()
                self.next?.handle(deviceName: deviceName, per: id.peripheralIdentifier, firmware: firmware)
                return true
            } else {
                print("Scan Handler - Didnt Find \(deviceName)")
                return false
            }
        }
    }
}
