import Foundation
import Bluejay

protocol Handler {
    var next: Handler? { get set }
    func handle(deviceName: String, per: PeripheralIdentifier?, firmware: Firmware)
}

extension Handler {
    mutating func setNext(next: Handler) {
        self.next = next
    }
}
