import Foundation
import Bluejay
struct ConnectionHandler: Handler {
    var next: Handler?
    
    func handle(deviceName: String, per: PeripheralIdentifier?, firmware: Firmware) {
        guard let id = per else { return }
        print("inside Connection Handler")
        OTAClient.shared.message("connecting to \(deviceName)")
        
        BLEManager.shared.connect(to: id) { result in
            switch result {
            case .success:
                print("Connection Handler - Successful connection to \(id)")
                if deviceName == firmware.advertName, firmware.advertName == "Modius" {
                    print("Handler waiting for you to manually pair, you have 10 seconds")
                    OTAClient.shared.message("Manually pair by pressing the button on headset, afterward press continue")
                    OTAClient.shared.dispatchAlert(action: {
                        self.next?.handle(deviceName: deviceName, per: id, firmware: firmware)
                    })
                } else {
                    next?.handle(deviceName: deviceName, per: id, firmware: firmware)
                }
                
            case .failure(let error):
                print("Connection Handler - Failed to connect to \(id) for \(error.localizedDescription)")
                return
            }
        }
    }
}
