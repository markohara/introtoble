import Foundation
import Bluejay

class DFUModeHandler: Handler {
    var next: Handler?
    var attempts = 0

    func handle(deviceName: String, per: PeripheralIdentifier?, firmware: Firmware) {
        print("inside DFU Handler")
        OTAClient.shared.message("Performing DFU write")
        BLEManager.shared.bluejay.write(to: otaCharacteristics.control.id, value: firmware.dfuPayload) { result in
            switch result {
            case .success:
                print("DFU Handler - Disconnected Successfully")
                if deviceName == firmware.advertName, firmware.advertName == "Modius" {
                    // Had to force disconnect for Slim because it doesnt automatically disconnect like sleep, only executes on initial scan when looking for "Modius"
                    BLEManager.shared.bluejay.disconnect(immediate: false, completion: nil)
                } else {
                    self.next?.handle(deviceName: deviceName, per: per, firmware: firmware)
                }
                
            default:
                // This is pretty much useless, if we dont get success here its likely something else is wrong
                print("Didnt Disconnect when writing DFU - attempt \(self.attempts)")
                self.attempts += 1
                if self.attempts <= 3 {
                    self.handle(deviceName: deviceName, per: per, firmware: firmware)
                }
            }
        }
    }
}
