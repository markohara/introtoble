import Foundation

// A mashup of both firmware and device to represent everything we need to know
enum Firmware: CaseIterable {
    internal enum Age: Int {
        case new, old
    }
    
    case modius(Age), sleep(Age)
    
    // All possible permutations, used to present list of avaliable firmware
    static var allCases: [Firmware] {
        [.modius(.new), modius(.old), sleep(.new), sleep(.old)]
    }
    
    var advertName: String {
        switch self {
        case .modius(_):
            return "Modius"
        case .sleep(_):
            return "Sleep"
        }
    }
    
    var bootloaderName: String {
        switch self {
        case .modius(_):
            return "ModiusBL"
        case .sleep(_):
            return "SleepBL"
        }
    }
    
    var filename: String {
        var result = ""
        switch self {
        case .modius(let age):
            result = ["Modius_OTA_v2.31RC_[BB99FF8A]", "Modius_OTA_v2.10RC"][age.rawValue]
        case .sleep(let age):
            result = ["Modius_210_sleep_2-0-5-RC_OTA", "Modius_210_sleep_2-0-3-RC_OTA"][age.rawValue]
        }
        
        return result
    }
    
    var name: String {
        var result = ""
        switch self {
        case .modius(let age):
            result = ["Modius OTA v2.31", "Modius OTA v2.10"][age.rawValue]
        case .sleep(let age):
            result = ["Sleep OTA v2.0.5", "Sleep OTA v2.0.3"][age.rawValue]
        }
        
        return result
    }
    
    // returns the corrosponding DFU payload based on device
    var dfuPayload: Data {
        switch self {
        case .modius:
            return Data(bytes: [UInt8(0)], count: 1)
        case .sleep:
            return Data(bytes: [UInt8(90),UInt8(90),UInt8(90),UInt8(90)], count: 4)
        }
    }
    
    // Both devices have a maximum byte size of 244 bits, how ever for writeWithoutResponse its actually 160 bits/ 20 bytes
    var MaximumByteSizeForDevice: Int {
        return 20
    }
    
    // Loads the firmware file from the bundle and returns it as a data object (Byte Array)
    func loadFirmware() -> Data? {
        if let path = Bundle.main.url(forResource: self.filename, withExtension: "ebl") {
            do {
                let data = try Data(contentsOf: path)
                return data
            } catch {
                print(error.localizedDescription)
            }
        }
        
        return nil
    }
}
