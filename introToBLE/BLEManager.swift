import Foundation
import Bluejay

class BLEManager {
    static let shared = BLEManager()
    let bluejay: Bluejay = Bluejay()
    
    private init() {
        self.bluejay.start()
    }
    
    func scan(completion: @escaping ([ScanDiscovery]) -> Bool) {
        bluejay.scan(duration: .infinity, allowDuplicates: true, throttleRSSIDelta: 0, serviceIdentifiers: []) { (discovery, discoveries) -> ScanAction in
            // The completion determines if we should continue scanning, results without names are omitted
            return completion(discoveries.filter{$0.peripheralIdentifier.name != "No Name"}) ? .stop : .continue
        } expired: { (discovery, discoveries) -> ScanAction in
            return .continue
        } stopped: { (discoveries, error) in
            if let error = error {
                debugPrint("Scan stopped with error: \(error.localizedDescription)")
            }
            else {
                debugPrint("Scan stopped without error.")
            }
        }

    }

    func connect(to id: PeripheralIdentifier,  completion: @escaping (ConnectionResult) -> Void) {
        bluejay.connect(id, timeout: .seconds(15)) { result in
            completion(result)
        }
    }
}
