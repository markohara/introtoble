import Foundation

extension Data {
    /// Takes Data and breaks it down into chunks of N length
    /// - Parameter chunkSize: Chunk size in bytes
    /// - Returns: Data broken down into a collection of chunks N in length
    func chunks(of chunkSize: Int) -> [Data] {
        let dataLen = self.count
        let fullChunks = Int(dataLen / chunkSize)
        let totalChunks = fullChunks + (dataLen % 1024 != 0 ? 1 : 0)
        
        var chunks:[Data] = [Data]()
        for chunkCounter in 0..<totalChunks {
            var chunk:Data
            let chunkBase = chunkCounter * chunkSize
            var diff = chunkSize
            if(chunkCounter == totalChunks - 1) {
                diff = dataLen - chunkBase
            }
            
            let range:Range<Data.Index> = chunkBase..<(chunkBase + diff)
            chunk = self.subdata(in: range)
            chunks.append(chunk)
        }
        
        return chunks
    }
}
